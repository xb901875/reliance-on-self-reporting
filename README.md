# Reliance on self reporting

Data and code repository for the manuscript *Reliance on self-reports and
estimated food composition data in nutrition research introduces
significant bias that can only be addressed with biomarkers*

Javier I. Ottaviani, Virag Sagi-Kiss, Hagen Schroeter, Gunter G. C. Kuhnle

https://www.biorxiv.org/content/10.1101/2023.10.26.564308v2

## Description
Code and data repositiory

* **Manuscript** - Manuscript in LaTeX format, style files
* **Food Composition** - food composition data for flavanols and nitrates and script
  to create Figure 1 (range of food composition)
* **Variability_Assessment** - Scripts to simulate the variability in food
  composition

*Please note* The data used for these scripts is not available in this
repository but needs to be requested from _EPIC Norfolk_

## Data request
Information about data request from EPIC Norfolk can be found here: 
https://www.epic-norfolk.org.uk/for-researchers/data-sharing/data-requests/

Information about the data to be requested are found in the relevant folder.


## License
MIT License

## Contact

Gunter Kuhnle g.g.kuhnle@reading.ac.uk
